# Archiving a website with wget
A quick guide on how to grab an entire site so you can do many thing one of which I maninly create
quick mock ups of templates before purchasing or even existing sites and create quick changes to
show clients how their sites will look.

I used wget, which is avaliable with almost all linux systems.

## Command
``` bash
wget --mirror -p --html-extension --convert-links -e robots=off -P . http://url-to-site
```

!!! info
    That command doesn't throttle the requests, so it could cause problems
    if the server has high load. Here's what that line does:

## Flags
* --mirror: turns on recursion, rather than just downloading the single file at the root of the URL, it'' now suck down the entire site
* -p: download all prerequistes (supporting media) rather than just the html
* --html-extension: this adds .html after the download filename, to make sure it plays nicely on whatever system you're going to view the archive on
* --convert-links: rewrite the URLs in the downloaded html files, to point to the downloaded files rather than to the live site. This makes it nice and
portable, with everthing living in a self-contained directory.
* -e robots=off: executes the "robots off" command, telling wget to ignore any directive to ignore the site in question. This is strictly not a good thing to do, but if you own the site, this is OK. If you don't own the site being archived, you should obey all robots.txt.
* -P .: set the download directory to something. I left it at the default "." (which means "here") but this is where you could pass in a directory path to tell wget to save the archived site. Handy, if you're doing this on a regular basis aka like a cron job.
* http://url-to-site: this is the full URL of the site to download. You'll likely want to change this

!!! tip
    You may also need to play around with the -D domain-list and/or --exclude-domains options, if you just want to control how it handles content hosted on more than one domain.